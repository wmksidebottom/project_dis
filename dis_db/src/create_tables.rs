use rusqlite::Connection;

pub struct SQLCommand {
    pub command_name: String,
    pub command_string: String,
}

pub fn create_table(conn: &Connection, create_table_cmd: &SQLCommand) {
    print!("{} - ", &create_table_cmd.command_name);

    match conn.execute(&create_table_cmd.command_string, ()) {
        Ok(_) => println!("Success!"),
        Err(err) => println!("Failed: {}", err)
    }
}

pub fn get_create_component_state_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Create Table: [ COMPONENT STATE ]"),
        command_string: String::from("CREATE TABLE IF NOT EXISTS component_state (
            state_id           INTEGER PRIMARY KEY,
            state              TEXT CHECK( state IN ('Offline','Idle','Running') ) NOT NULL DEFAULT 'Offline')"
        )
    }
}

pub fn get_create_component_status_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Create Table: [ COMPONENT STATUS ]"),
        command_string: String::from("CREATE TABLE IF NOT EXISTS component_status (
            status_id           INTEGER PRIMARY KEY,
            status              TEXT CHECK( status IN ('Broken','Repairing','Working') ) NOT NULL DEFAULT 'Working',
            value               FLOAT)"
        )
    }
}

pub fn get_create_radar_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Create Table: [ RADAR ]"),
        command_string: String::from(
            "CREATE TABLE IF NOT EXISTS radar (
            id                          INTEGER PRIMARY KEY,
            model                       VARCHAR(50),
            weight                      FLOAT(24),
            area                        FLOAT(24),
            status_id                   INTEGER NOT NULL,
            transmitter_power           FLOAT(24),
            transmitter_gain            FLOAT(24),
            fitting_id                  INTEGER,
            state_id                    INTEGER NOT NULL,
            inventory_id                INTEGER,
            FOREIGN KEY (status_id)     REFERENCES component_status(status_id),
            FOREIGN KEY (state_id)      REFERENCES component_state(state_id),
            FOREIGN KEY (fitting_id)    REFERENCES fitting(fitting_id),
            FOREIGN KEY (inventory_id)  REFERENCES inventory(inventory_id)
        )",
        ),
    }
}

pub fn get_create_radio_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Create Table: [ RADIO ]"),
        command_string: String::from(
            "CREATE TABLE IF NOT EXISTS radio (
            id                          INTEGER PRIMARY KEY,
            model                       VARCHAR(50),
            weight                      FLOAT(24),
            area                        FLOAT(24),
            status_id                   INTEGER NOT NULL,
            transmitter_power           FLOAT(24),
            transmitter_gain            FLOAT(24),
            fitting_id                  INTEGER,
            state_id                    INTEGER NOT NULL,
            inventory_id                INTEGER,
            FOREIGN KEY (status_id)     REFERENCES component_status(status_id),
            FOREIGN KEY (state_id)      REFERENCES component_state(state_id),
            FOREIGN KEY (fitting_id)    REFERENCES fitting(fitting_id),
            FOREIGN KEY (inventory_id)  REFERENCES inventory(inventory_id)
        )",
        ),
    }
}

pub fn get_create_fitting_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Creating Table: [ FITTING ]"),
        command_string: String::from(
            "CREATE TABLE IF NOT EXISTS fitting (
                fitting_id      INTEGER PRIMARY KEY,
                capacity        TINYINT(255),
                available_slots TINYINT(255))",
        ),
    }
}


pub fn get_create_inventory_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Creating Table: [ INVENTORY ]"),
        command_string: String::from(
            "CREATE TABLE IF NOT EXISTS inventory (
                inventory_id    INTEGER PRIMARY KEY,
                current_weight  FLOAT(24),
                maximum_space   FLOAT(24),
                occupied_space  FLOAT(24),
                available_space FLOAT(24))",
        ),
    }
}

pub fn get_create_corvette_table_cmd() -> SQLCommand {
    SQLCommand {
        command_name: String::from("Creating Table: [ CORVETTE ]"),
        command_string: String::from("CREATE TABLE IF NOT EXISTS corvettes (
            corvette_id     INTEGER PRIMARY KEY,
            class           VARCHAR(50),
            model           TEXT CHECK( model IN ('Razorback','BlueFalcon','Argonautica') ) NOT NULL DEFAULT 'Razorback',
            callsign        VARCHAR(50),
            weight          Float(20),
            fitting_id      INTEGER NOT NULL,
            inventory_id    INTEGER NOT NULL,
            FOREIGN KEY (fitting_id)       REFERENCES fitting(fitting_id),
            FOREIGN KEY (inventory_id)     REFERENCES inventory(inventory_id))"
        )    
    }
}