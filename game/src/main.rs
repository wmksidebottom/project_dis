use behaviors;

use behaviors::entities::ships::corvette::{Corvette , CorvetteModel};
use behaviors::components::Component;
use behaviors::components::radar::Radar;
use behaviors::components::radio::Radio;

fn main() {
    let new_ship_callsign: String = String::from("SunStrider");
    let new_corvette_model: CorvetteModel = CorvetteModel::Razorback;

    let mut sun_strider: Corvette = Corvette::new(new_corvette_model, new_ship_callsign);
    println!("");

    let bullseye_radar: Radar = Radar::new(String::from("AN/APG-81"));
    let uhf_radio: Radio = Radio::new(String::from("UHF"));
    let vhf_radio: Radio = Radio::new(String::from("VHF"));
    let fire_control_radar: Radar = Radar::new(String::from("AN/ALQ-99"));
    let sincgars: Radio = Radio::new(String::from("SINCGARS"));
    let havequick: Radio = Radio::new(String::from("HaveQuick"));

    let components_list: Vec<Component> = vec![
        Component::Radar(bullseye_radar),
        Component::Radio(uhf_radio),
        Component::Radio(vhf_radio),
        Component::Radar(fire_control_radar),
        Component::Radio(sincgars),
        Component::Radio(havequick),
    ];

    let installation_results = sun_strider.fitting.install_component_list(components_list);

    println!("");

    match installation_results {
        Result::Ok(_) => println!("Locked and loaded!"),
        Result::Err(spare_components) => {
            println!(
                "Uh oh, it looks we tried to install too many components on our {:?}",
                &sun_strider.model
            );
            println!("Lets put the rest in our inventory!\n");
            let _inventory_result = sun_strider.inventory.add_item_list(spare_components);
        }
    }

    println!("");
    sun_strider.fitting.print_fitting();
    
    println!("");
    sun_strider.inventory.print_inventory();

}
