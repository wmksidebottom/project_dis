pub mod radar;
pub mod radio;

use crate::components::radar::Radar;
use crate::components::radio::Radio;
use crate::physics::si_units::{Kilogram, SquareMeter};

use std::fmt;

/// A enumeration to represent a component slot where components can be installed (on a ship)
#[derive(Debug, PartialEq, Clone)]
pub enum ComponentSlot {
    Empty,
    Installed(Component),
}

impl Default for ComponentSlot {
    fn default() -> Self {
        ComponentSlot::Empty
    }
}

/// An Enumeration for defining various kinds of components such as a radar or a radio
#[derive(Debug, PartialEq, Clone)]
pub enum Component {
    Radio(Radio),
    Radar(Radar),
    // IFF,
    // Jammer,
    // Core,
    // Drive
}

impl Component {
    /// Return the model name of the specified components
    pub fn get_name(&self) -> &str {
        match &self {
            Component::Radar(radar) => &radar.model,
            Component::Radio(radio) => &radio.model,
        }
    }

    /// Return the component status of the specified component
    pub fn get_status(&self) -> &ComponentStatus {
        match &self {
            Component::Radar(radar) => &radar.status,
            Component::Radio(radio) => &radio.status,
        }
    }

    /// Return the component state of the specified component
    pub fn get_state(&self) -> &ComponentState {
        match &self {
            Component::Radar(radar) => &radar.state,
            Component::Radio(radio) => &radio.state,
        }
    }

    pub fn get_area(&self) -> &SquareMeter {
        match &self {
            Component::Radar(radar) => &radar.area,
            Component::Radio(radio) => &radio.area,
        }
    }

    pub fn get_weight(&self) -> &Kilogram {
        match &self {
            Component::Radar(radar) => &radar.weight,
            Component::Radio(radio) => &radio.weight,
        }
    }
}

impl fmt::Display for Component {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Component::Radio(_) => write!(f, "Radio"),
            Component::Radar(_) => write!(f, "Radar"),
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ComponentState {
    Offline,
    Idle,
    Running,
}

impl Default for ComponentState {
    fn default() -> Self {
        ComponentState::Idle
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ComponentStatus {
    Destroyed,      // Needs to be repaired in shop or replaced
    Broken(f32),    // below 25% functional - needs to be repaired to turn on
    Repairing(f32), // todo: how long will it take to repair
    Working(f32),   // Show current health
}

// Implement the Default Trait on ComponentStatus to provide a default value
impl Default for ComponentStatus {
    fn default() -> Self {
        ComponentStatus::Working(100.0)
    }
}