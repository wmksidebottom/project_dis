/// Fitting abstraction
/// 
/// Ships will ideally have four distinct fittings which maintain a collection
/// of component slots for each site of the ship:
/// - The Keel Fitting
/// - The Starboard Fitting
/// - The Portside Fitting
/// - The Deck Fitting 
/// 
pub mod fitting;
pub mod inventory;