use crate::components::Component;
use crate::physics::si_units::{Kilogram, SquareMeter};


/// A class which represents an ship's inventory for storing items
#[derive(Debug)]
pub struct Inventory {
    pub current_weight: Kilogram,
    pub maximum_space: SquareMeter,
    pub occupied_space: SquareMeter,
    pub available_space: SquareMeter,
    pub item_list: Vec<Component>,
}

impl Inventory {
    /// Create a new inventory by providing the maximum space desired
    pub fn new(max_space: SquareMeter) -> Inventory {
        let new_inventory: Inventory = Inventory {
            current_weight: Kilogram(0.0),
            maximum_space: max_space.clone(),
            occupied_space: SquareMeter(0.0),
            available_space: max_space,
            item_list: vec![],
        };

        return new_inventory;
    }

    /// Print the ship's current inventory
    pub fn print_inventory(&self) {
        println!("Listing current inventory:");
        println!("Current weight: {:?}", self.current_weight);
        println!("Maximum space: {:?}", self.maximum_space);
        println!("Occupied space: {:?}", self.occupied_space);
        println!("Available space: {:?}", self.available_space);

        for (item_num, item) in self.item_list.iter().enumerate() {
            println!("Item number {}: [ {:?} ]", item_num, item.get_name());
        }
    }

    /// Add an item to the inventory, if there's available space for it.
    pub fn add_item(&mut self, item: Component) -> Result<(), Component> {
        // Guard: No more space available
        if self.available_space == SquareMeter(0.0) {
            eprintln!("Inventory: No available space!");
            return Err(item);
        }

        // Guard: Not enough space for particular item
        if &self.available_space - item.get_area() < SquareMeter(0.0) {
            eprintln!("Inventory: Item is to large for current inventory capacity!");
            return Err(item);
        }

        // Buzz lightyear saying: "Lifetimes"
        self.current_weight = &self.current_weight + item.get_weight();
        self.occupied_space = &self.occupied_space + item.get_area();
        self.available_space = &self.available_space - item.get_area();

        println!("Adding item to inventory: [ {} ]", item.get_name());

        self.item_list.push(item);

        return Ok(());
    }

    /// Add a list of items to the inventory
    pub fn add_item_list(&mut self, item_list: Vec<Component>) -> Result<(), Vec<Component>> {
        let items_not_installed: Vec<Component> = item_list
            .into_iter()
            .map(|c| self.add_item(c))
            .filter_map(|r| r.err())
            .collect();

        match &items_not_installed.len() {
            0 => return Ok(()),
            _ => return Err(items_not_installed),
        }
    }
}

#[cfg(test)]
mod inventory_tests {
    use crate::components::Component;
    use crate::components::radio::Radio;

    use super::*;

    #[test]
    fn create_inventory() {
        let inventory_space = SquareMeter(1000.0);
        let test_inventory: Inventory = Inventory::new(inventory_space);

        assert_eq!(SquareMeter(1000.0), test_inventory.maximum_space);
        assert_eq!(SquareMeter(1000.0), test_inventory.available_space);
        assert_eq!(SquareMeter(0.0), test_inventory.occupied_space);
        assert_eq!(Kilogram(0.0), test_inventory.current_weight);
        assert!(test_inventory.item_list.is_empty());
    }

    #[test]
    fn add_item_to_inventory_with_available_space() {
        let inventory_space = SquareMeter(1000.0);

        let mut test_inventory: Inventory = Inventory::new(inventory_space);

        let test_radio = Component::Radio(Radio::new(String::from("test model")));

        let test_result = test_inventory.add_item(test_radio);

        assert!(test_result.is_ok());
        assert_eq!(SquareMeter(999.0), test_inventory.available_space);
        assert_eq!(SquareMeter(1.0), test_inventory.occupied_space);
    }

    #[test]
    fn add_item_to_inventory_with_no_available_space() {
        let inventory_space = SquareMeter(0.5);

        let mut test_inventory: Inventory = Inventory::new(inventory_space);

        let test_radio = Component::Radio(Radio::new(String::from("test model")));

        let test_result = test_inventory.add_item(test_radio);

        assert!(test_result.is_err());
    }
}
