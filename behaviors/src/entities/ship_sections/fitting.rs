use crate::components::{Component, ComponentSlot};

#[derive(Debug)]
pub struct Fitting {
    capacity: usize,
    available_slots: usize,
    slots: Vec<ComponentSlot>,
}

impl Fitting {
    pub fn new(new_capacity: usize) -> Fitting {
        let new_fitting: Fitting = Fitting {
            capacity: new_capacity,
            available_slots: new_capacity,
            slots: vec![ComponentSlot::Empty; new_capacity],
        };

        return new_fitting;
    }

    pub fn print_component_slot_status(&self, slot_number: usize) {
        if slot_number > self.capacity {
            eprintln!("Slot number beyond fitting capacity!")
        }

        match &self.slots[slot_number] {
            ComponentSlot::Empty => println!("\t - Slot Number {}: Empty", slot_number),
            ComponentSlot::Installed(component) => {
                println!(
                    "Slot Number {}: [ {} ] State: [ {:?} ] Status: [ {:?} ]",
                    &slot_number,
                    component.get_name(),
                    component.get_state(),
                    component.get_status()
                )
            }
        }
    }

    pub fn print_fitting(&self) {
        println!("Printing ship fitting: ");
        
        for slot_number in 0..self.capacity {
            self.print_component_slot_status(slot_number);
        }
    }

    pub fn install_component(&mut self, new_component: Component) -> Result<(), Component> {
        let component_slots = 0..self.capacity;

        let first_available_slot = component_slots
            .into_iter()
            .find(|&x| self.slots[x] == ComponentSlot::Empty);

        match first_available_slot {
            None => {
                eprintln!(
                    "No Available Slots -- Unable to install component: [ {} - {} ].",
                    &new_component,
                    &new_component.get_name()
                );
                return Err(new_component);
            }
            Some(slot_number) => {
                println!("Installing Component: ");
                println!(
                    " - Slot {}: [ {} - {} ]",
                    &slot_number,
                    &new_component,
                    &new_component.get_name()
                );
                self.slots[slot_number] = ComponentSlot::Installed(new_component.clone());
                self.available_slots -= 1;
                return Ok(());
            }
        }
    }

    pub fn install_component_list(
        &mut self,
        component_list: Vec<Component>,
    ) -> Result<(), Vec<Component>> {
        let components_not_installed: Vec<Component> = component_list
            .into_iter()
            .map(|c| self.install_component(c))
            .filter_map(|r| r.err())
            .collect();
        match &components_not_installed.len() {
            0 => return Ok(()),
            _ => return Err(components_not_installed),
        }
    }

    pub fn remove_component(&mut self, slot_number: usize) -> Result<Component, usize> {
        if slot_number > self.capacity {
            eprintln!(
                "Slot number [ {} ] is currently outside fitting's capacity!",
                &slot_number
            );
            return Err(slot_number);
        }

        let removed_component_slot =
            std::mem::replace(&mut self.slots[slot_number], ComponentSlot::Empty);

        match removed_component_slot {
            ComponentSlot::Empty => {
                eprintln!("Slot number [ {} ] is currently empty!", &slot_number);
                return Err(slot_number);
            }
            ComponentSlot::Installed(component_type) => {
                self.available_slots += 1;
                println!(
                    "Removing Component: [ {:?} ] for Slot number [ {:?} ]",
                    &component_type, &slot_number
                );
                return Ok(component_type);
            }
        }
    }

    pub fn remove_all_components(&mut self) -> Option<Vec<Component>> {
        // Create a range of slots
        let component_slots = 0..self.capacity;

        // Map array of component slots => vector of results
        // filter and map vector of results to results that are ok and return
        let removed_components: Vec<Component> = component_slots
            .into_iter()
            .map(|s| self.remove_component(s))
            .filter_map(|r| r.ok())
            .collect();

        match removed_components.len() {
            0 => return None,
            _ => return Some(removed_components),
        }
    }
}

#[cfg(test)]
mod fitting_tests {
    use crate::components::radio::Radio;

    use super::*;

    #[test]
    fn install_component_on_fitting_with_available_slot() {
        let mut test_fitting: Fitting = Fitting::new(4);
        let test_radio = Radio::default();
        let installation_result = test_fitting.install_component(Component::Radio(test_radio));

        assert!(&installation_result.is_ok());
        assert_eq!((), installation_result.unwrap());
        assert_eq!(test_fitting.available_slots, 3);
    }

    #[test]
    fn install_component_on_fitting_without_available_slots() {
        let mut test_fitting: Fitting = Fitting::new(4);
        let test_radio1 = Radio::default();
        let test_radio2 = Radio::default();
        let test_radio3 = Radio::default();
        let test_radio4 = Radio::default();
        let test_radio5 = Radio::default();
        let _installation_result1 = test_fitting.install_component(Component::Radio(test_radio1));
        let _installation_result2 = test_fitting.install_component(Component::Radio(test_radio2));
        let _installation_result3 = test_fitting.install_component(Component::Radio(test_radio3));
        let _installation_result4 = test_fitting.install_component(Component::Radio(test_radio4));
        let installation_result5 = test_fitting.install_component(Component::Radio(test_radio5));

        assert!(&installation_result5.is_err());
        assert_eq!(
            Component::Radio(Radio::default()),
            installation_result5.unwrap_err()
        );
    }

    #[test]
    fn remove_component_from_fitting_slot() {
        let mut test_fitting: Fitting = Fitting::new(4);
        let test_radio = Radio::default();
        let _installation_result = test_fitting.install_component(Component::Radio(test_radio));

        let removal_result = test_fitting.remove_component(0);

        assert!(&removal_result.is_ok());
        assert_eq!(Component::Radio(Radio::default()), removal_result.unwrap());
    }

    #[test]
    fn remove_component_from_empty_fitting_slot() {
        let mut test_fitting: Fitting = Fitting::new(4);

        let slot_number = 0;

        let removal_result = test_fitting.remove_component(slot_number);

        assert!(&removal_result.is_err());
        assert_eq!(slot_number, removal_result.unwrap_err());
    }

    #[test]
    fn remove_all_components_from_empty_fitting() {
        let mut test_fitting: Fitting = Fitting::new(4);

        let removal_result = test_fitting.remove_all_components();

        assert!(&removal_result.is_none());
    }

    #[test]
    fn remove_all_components_from_fitting() {
        let mut test_fitting: Fitting = Fitting::new(4);

        let test_radio1 = Radio::default();
        let test_radio2 = Radio::default();
        let test_radio3 = Radio::default();
        let test_radio4 = Radio::default();
        let _installation_result1 = test_fitting.install_component(Component::Radio(test_radio1));
        let _installation_result2 = test_fitting.install_component(Component::Radio(test_radio2));
        let _installation_result3 = test_fitting.install_component(Component::Radio(test_radio3));
        let _installation_result4 = test_fitting.install_component(Component::Radio(test_radio4));

        let removal_result = test_fitting.remove_all_components();

        let test_radio5 = Radio::default();
        let test_radio6 = Radio::default();
        let test_radio7 = Radio::default();
        let test_radio8 = Radio::default();

        let answer: Vec<Component> = vec![
            Component::Radio(test_radio5),
            Component::Radio(test_radio6),
            Component::Radio(test_radio7),
            Component::Radio(test_radio8),
        ];

        assert!(&removal_result.is_some());
        assert_eq!(answer, removal_result.unwrap());
    }

    #[test]
    fn install_component_list_on_fitting_with_available_slots() {
        let mut test_fitting: Fitting = Fitting::new(4);

        let test_radio1 = Component::Radio(Radio::default());
        let test_radio2 = Component::Radio(Radio::default());
        let test_radio3 = Component::Radio(Radio::default());
        let test_radio4 = Component::Radio(Radio::default());

        let test_component_list: Vec<Component> =
            vec![test_radio1, test_radio2, test_radio3, test_radio4];

        let installation_result = test_fitting.install_component_list(test_component_list);

        assert!(&installation_result.is_ok());
        assert_eq!(Ok(()), installation_result);
    }

    #[test]
    fn install_component_list_on_fitting_with_few_available_slots() {
        let mut test_fitting: Fitting = Fitting::new(2);

        let test_radio1 = Component::Radio(Radio::default());
        let test_radio2 = Component::Radio(Radio::default());
        let test_radio3 = Component::Radio(Radio::default());
        let test_radio4 = Component::Radio(Radio::default());

        let test_component_list: Vec<Component> =
            vec![test_radio1, test_radio2, test_radio3, test_radio4];

        let installation_result = test_fitting.install_component_list(test_component_list);

        assert!(&installation_result.is_err());
        assert_eq!(
            Err(vec![
                Component::Radio(Radio::default()),
                Component::Radio(Radio::default())
            ]),
            installation_result
        );
    }
}
