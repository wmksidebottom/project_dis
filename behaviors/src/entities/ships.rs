pub mod corvette;

#[derive(Debug, Default)]
pub enum ShipClass {
    #[default]
    Corvette,
    Frigate,
    Destroyer,
}

pub trait Ship {}
