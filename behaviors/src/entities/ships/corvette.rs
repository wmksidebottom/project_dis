use crate::entities::ships::{Ship, ShipClass};
use crate::entities::{Entity, EntityKind};
use crate::entities::ship_sections::fitting::Fitting;
use crate::entities::ship_sections::inventory::Inventory;
use crate::physics::kinematics::{Acceleration, Position, Velocity};
use crate::physics::si_units::{Kilogram, SquareMeter};

#[derive(Debug, Default)]
pub enum CorvetteModel {
    #[default]
    Razorback,
    BlueFalcon,
    Argonautica,
}

#[derive(Debug)]
pub struct Corvette {
    // Identifiers
    pub class: EntityKind,
    pub model: CorvetteModel,
    pub callsign: String,
    // Structure
    pub weight: Kilogram,
    pub fitting: Fitting,
    pub inventory: Inventory,
    // Kinematics
    pub current_position: Position,
    pub current_velocity: Velocity,
    pub current_accleration: Acceleration,
}

impl Corvette {
    pub fn new(corvette_model: CorvetteModel, corvette_callsign: String) -> Corvette {
        let new_corvette_hull: Corvette = Corvette {
            class: EntityKind::Ship(ShipClass::Corvette),
            model: corvette_model,
            callsign: corvette_callsign,
            weight: Kilogram(100000.0),
            fitting: Fitting::new(4),
            inventory: Inventory::new(SquareMeter(100.0)),
            current_position: Position { x: 0.0, y: 0.0 },
            current_velocity: Velocity { x: 0.0, y: 0.0 },
            current_accleration: Acceleration { x: 0.0, y: 0.0 },
        };

        println!(
            "Ship built -- Class: [ {:?} ] Model: [ {:?} ] Callsign: [ {:?} ]",
            &new_corvette_hull.class, &new_corvette_hull.model, &new_corvette_hull.callsign
        );

        return new_corvette_hull;
    }
}

impl Entity for Corvette {
    fn get_name(&self) -> &str {
        return &self.callsign;
    }
    fn get_position(&self) -> &Position {
        return &self.current_position;
    }
}

impl Ship for Corvette {}
