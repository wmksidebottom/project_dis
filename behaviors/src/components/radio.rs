use crate::physics::si_units::{Kilogram, SquareMeter};
use crate::components::{ComponentState, ComponentStatus};

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Radio {
    pub model: String,
    pub weight: Kilogram,
    pub area: SquareMeter,
    pub status: ComponentStatus,
    pub state: ComponentState,
    pub transmitter_power: f32,
    pub transmitter_gain: f32,
}

impl Radio {
    pub fn new(model_name: String) -> Radio {
        let new_radio: Radio = Radio {
            model: model_name,
            weight: Kilogram(10.0),
            area: SquareMeter(1.0),
            status: ComponentStatus::Working(100.0),
            state: ComponentState::Offline,
            transmitter_power: 35.0,
            transmitter_gain: 60.0,
        };

        return new_radio;
    }
}