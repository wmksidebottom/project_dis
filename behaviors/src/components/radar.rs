use crate::physics::si_units::{Kilogram, SquareMeter};
use crate::components::{ComponentState, ComponentStatus};

// Build a Radar. See https://en.wikipedia.org/wiki/Radar#Radar_signal for more
#[derive(Debug, Default, PartialEq, Clone)]
pub struct Radar {
    pub model: String,
    pub weight: Kilogram,
    pub area: SquareMeter,
    pub status: ComponentStatus,
    pub state: ComponentState,
    pub transmitter_power: f32,
    pub transmitter_gain: f32,
}

impl Radar {
    pub fn new(model_name: String) -> Radar {
        let new_radar: Radar = Radar {
            model: model_name,
            weight: Kilogram(10.0),
            area: SquareMeter(1.0),
            status: ComponentStatus::Working(100.0),
            state: ComponentState::Offline,
            transmitter_power: 50.0,
            transmitter_gain: 50.0,
        };

        return new_radar;
    }
}