pub mod ships;
pub mod ship_sections;

use crate::entities::ships::ShipClass;
use crate::physics::kinematics::Position;

// Define Entity Kind Enum Hierarchy
#[derive(Debug)]
pub enum EntityKind {
    Planet(String),
    Ship(ShipClass),
    Satellite(String),
    SpaceStation(String),
}

impl Default for EntityKind {
    fn default() -> Self {
        EntityKind::Ship(ShipClass::Corvette)
    }
}

// Define Entity and Ship Traits
pub trait Entity {
    fn get_name(&self) -> &str;
    fn get_position(&self) -> &Position;
}
