#[derive(Debug, PartialEq, Clone)]
pub struct Kilogram(pub f32);

impl Default for Kilogram {
    fn default() -> Self {
        Kilogram(100000.0)
    }
}

impl std::ops::Add for Kilogram {
    type Output = Kilogram;

    fn add(self, rhs: Self) -> Self::Output {
        return Kilogram(self.0 + rhs.0);
    }
}

impl<'a, 'b> std::ops::Add<&'b Kilogram> for &'a Kilogram {
    type Output = Kilogram;

    fn add(self, rhs: &'b Kilogram) -> Self::Output {
        return Kilogram(self.0 + rhs.0);
    }
}

impl std::ops::Sub for Kilogram {
    type Output = Kilogram;

    fn sub(self, rhs: Self) -> Self::Output {
        return Kilogram(self.0 - rhs.0);
    }
}

impl<'a, 'b> std::ops::Sub<&'b Kilogram> for &'b Kilogram {
    type Output = Kilogram;

    fn sub(self, rhs: &'b Kilogram) -> Self::Output {
        return Kilogram(self.0 - rhs.0);
    }
}

#[derive(Debug)]
pub struct Meter(pub f32);

impl Default for Meter {
    fn default() -> Self {
        Meter(0.0)
    }
}

#[derive(Debug)]
pub struct Newton(pub f32);

impl Default for Newton {
    fn default() -> Self {
        Newton(0.0)
    }
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct SquareMeter(pub f32);

impl Default for SquareMeter {
    fn default() -> Self {
        SquareMeter(0.0)
    }
}

impl std::ops::Add for SquareMeter {
    type Output = SquareMeter;

    fn add(self, rhs: SquareMeter) -> SquareMeter {
        return SquareMeter(self.0 + rhs.0);
    }
}

// Lifetimes a and b
// We can think of a as the lifetime of the caller and b as the lifetime
// of the reference passed into the function
// Note we want the lifetime of what is returned to be that of the lifetime
// of the caller, a.
impl<'a, 'b> std::ops::Add<&'b SquareMeter> for &'a SquareMeter {
    type Output = SquareMeter;

    fn add(self, rhs: &'b SquareMeter) -> Self::Output {
        return SquareMeter(self.0 + rhs.0);
    }
}

impl std::ops::Sub for SquareMeter {
    type Output = SquareMeter;

    fn sub(self, rhs: Self) -> Self::Output {
        return SquareMeter(self.0 - rhs.0);
    }
}

impl<'a, 'b> std::ops::Sub<&'b SquareMeter> for &'a SquareMeter {
    type Output = SquareMeter;

    fn sub(self, rhs: &'b SquareMeter) -> Self::Output {
        return SquareMeter(self.0 - rhs.0);
    }
}
