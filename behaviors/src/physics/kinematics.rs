#[derive(Debug)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

impl Default for Position {
    fn default() -> Self {
        Position { x: 0.0, y: 0.0 }
    }
}

#[derive(Debug)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}

impl Default for Velocity {
    fn default() -> Self {
        Velocity { x: 0.0, y: 0.0 }
    }
}

#[derive(Debug)]
pub struct Acceleration {
    pub x: f32,
    pub y: f32,
}

impl Default for Acceleration {
    fn default() -> Self {
        Acceleration { x: 0.0, y: 0.0 }
    }
}

// TODO: Implement angular kinematics
