# Project DIS

## Description

The purpose of this project is to illustrate a typical DevOps lifecycle for a simple game / simulation written in Rust. The game will revolve around controlling a single entity in 2-D space to complete missions. The game will (loosely) utilize the Distributed Interactive Simulation (DIS) Protocol to handle state information for any entity within the game. As development proceeds, other objects like planets and asteroid belts will be introduced which orbit around the solar system for the Player to explore (Examples: Manually set a terminal velocity, set an acceleration, Set destination for a planet or a moon, calculate trajectory, orbit a target, handle collisions)

## Documentation

The auto-generated docs for this project can be found in the below links:
- [dis_db](https://wmksidebottom.gitlab.io/project_dis/doc/dis_db/)
- [behaviors](https://wmksidebottom.gitlab.io/project_dis/doc/behaviors/)
- [game](https://wmksidebottom.gitlab.io/project_dis/doc/game/)

## Visuals

TODO

## Installation

TODO

## Usage

TODO

## Roadmap

### Short Term

- [ ] Implement new() constructors for all structs and enums
- [ ] Move println statements to be strictly debugging statements
- [ ] Organize structures and enums into their own files for better maintenance
- [ ] Implement basic units tests for structs / enum functions
- [ ] Move Ship and Entity Trait into their own Procedural Macros
- [ ] Continue implmenting various kinematics + SI unit packages

### Long Term
- [ ] Implement Movement (Position, Velocity, and Acceleration) for Entities
    - [ ] Include force and angular equivalents
- [ ] Implement Basic EM/ECM Components (Radio, Radar, Jammer)
- [ ] Create Database with entities and components
- [ ] Create Network API to generate PDUs

## Contributing

TODO

## Authors and Acknowledgment

1. [William Sidebottom](https://gitlab.com/wmksidebottom)

## License

[Project DIS License](LICENSE)

## Project status

*Active*